import JsonSchema from './JsonSchema.vue'

export default ({ Vue }) => {
  Vue.component('JsonSchema', JsonSchema)
}
