const { path } = require('@vuepress/shared-utils')

module.exports = (options = {}, context) => {
  const baseUrl = options.baseURL || ''
  const title = options.title || ''
  const socials = options.socials || []

  return {
    define () {
      return {
        BASE_URL: baseUrl
      }
    },
    extendPageData ($page) {
      const { themeConfig } = context.siteConfig
      const language = themeConfig.language || ''
      const author = themeConfig.author || ''
      const schema = {
        '@context': 'http://schema.org',
        '@graph': []
      }
      let organization
      let website
      let webpage

      if (title) {
        organization = {
          '@type': 'Organization',
          '@id': baseUrl + '/#organization',
          name: title,
          url: baseUrl,
          logo: {
            '@type': 'ImageObject',
            '@id': baseUrl + '/#logo',
            url: baseUrl + '/img/resized/512x512-logo.png',
            width: 512,
            height: 512,
            caption: title
          },
          image: {
            '@id': baseUrl + '/#logo'
          }
        }

        if (socials.length > 0) {
          organization.sameAs = socials
        }

        schema['@graph'].push(organization)

        website = {
          '@type': 'WebSite',
          '@id': baseUrl + '/#website',
          url: baseUrl,
          name: title,
          publisher: {
            '@id': organization['@id']
          }
        }

        schema['@graph'].push(website)
      }

      if ($page.frontmatter.lang || language) {
        webpage = {
          '@type': 'WebPage',
          '@id': 'REPLACE_WITH_PAGE_URL#webpage',
          url: 'REPLACE_WITH_PAGE_URL',
          inLanguage: $page.frontmatter.lang || language
        }

        if (organization) {
          webpage.about = {
            '@id': organization['@id']
          }
        }

        if (website) {
          webpage.isPartOf = {
            '@id': website['@id']
          }
        }

        schema['@graph'].push(webpage)
      }

      if ($page._filePath && $page._filePath.includes('_posts') && $page.frontmatter) {
        const published = $page.frontmatter.date ? new Date($page.frontmatter.date).toISOString() : ''
        let image
        let person
        let article

        if ($page.frontmatter.image) {
          image = {
            '@type': 'ImageObject',
            '@id': 'REPLACE_WITH_PAGE_URL#primaryimage',
            url: $page.frontmatter.image
          }

          schema['@graph'].push(image)
        }

        if ($page.frontmatter.author || author) {
          person = {
            '@type': 'Person',
            '@id': baseUrl + '/#person',
            name: $page.frontmatter.author || author
          }

          schema['@graph'].push(person)
        }

        if (organization && webpage && image && person && published && $page.frontmatter.title && $page.frontmatter.description) {
          const dateModified = $page.lastUpdated ? new Date($page.lastUpdated).toISOString() : published

          article = {
            '@context': 'http://schema.org',
            '@type': 'Article',
            mainEntityOfPage: {
              '@id': webpage['@id']
            },
            headline: $page.frontmatter.title,
            description: $page.frontmatter.description,
            datePublished: published,
            image: {
              '@id': image['@id']
            },
            publisher: {
              '@id': organization['@id']
            },
            author: {
              '@id': person['@id']
            }
          }

          if (dateModified) {
            article.dateModified = dateModified
          }

          schema['@graph'].push(article)
        }
      }

      $page.schema = JSON.stringify(schema)
    },
    enhanceAppFiles: path.resolve(__dirname, 'enhanceAppFile.js'),
    globalUIComponents: 'JsonSchema'
  }
}
